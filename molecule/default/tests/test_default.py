import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_kernel_rt(host):
    cmd = host.run("uname -a")
    assert "PREEMPT RT" in cmd.stdout


def test_dkms_service_enabled_and_running(host):
    service = host.service("dkms")
    assert service.is_running
    assert service.is_enabled


def test_cmdline_parameter(host):
    cmd = host.run("cat /proc/cmdline")
    for param in (
        "isolcpus=0",
        "idle=poll",
        "intel_idle.max_cstate=0",
        "processor.max_cstate=1",
        "skew_tick=1",
        "clocksource=tsc",
    ):
        assert param in cmd.stdout
